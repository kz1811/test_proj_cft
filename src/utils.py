import bcrypt
from operations.bd import engine
from sqlmodel import create_engine, SQLModel, Session, select, or_
from operations.models import UserLoginModel, UserCredentials, Salary, UserRegistrationModel

def check_user(data: UserLoginModel):
    with Session(engine) as session:
        query = select(UserCredentials)
        curr_cred = data.password.encode("utf-8")
        query = query.where(UserCredentials.username == data.username)
        answer = session.exec(query).all()
        ### Если запись с таким username не найдена
        if answer == []: 
            return False
        else:
            if bcrypt.checkpw(curr_cred, answer[0].password.encode("utf-8")): # Если запись найдена, но хеши паролей не совпадают
                return answer[0].id
        return False 

def does_user_exists(username: str, email: str) -> bool:
    with Session(engine) as session:
        query = select(UserCredentials).where(or_(UserCredentials.username == username, UserCredentials.email == email))
        answer = session.exec(query).all()
        return answer != []

def get_salary_from_db(user_id: str):
    with Session(engine) as session:
        query = select(UserCredentials).where(UserCredentials.id == user_id)
        user_credentials = session.exec(query).first()
        if user_credentials:
            return user_credentials.salary
    return None