from sqlmodel import create_engine

DATABASE_URL = "sqlite:///app.db"

engine = create_engine(
    DATABASE_URL,
    connect_args={
        "check_same_thread": False # For SQLite, no other databases
    },
    echo=True # Log generated SQL
)