import json
from datetime import datetime
from sqlmodel import SQLModel, Field, Relationship
from pydantic import BaseModel, validator

class UserRegistrationModel(BaseModel):
    email: str = Field(unique=True)
    username: str = Field(unique=True)
    password: str = Field(...)
    salary: int = Field(...)
    date: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "abc@index.com",
                "username": "username",
                "password": "password",
                "salary": 0,
                "date": "dd.mm.yyyy"
            }
        }

    @validator("date")
    def validate_date(cls, value):
        try:
            datetime.strptime(value, "%d.%m.%Y")
            return value
        except ValueError:
            raise ValueError("Invalid date format. Expected format: dd.mm.yyyy")


class UserLoginModel(SQLModel):
    username: str = Field(...)
    password: str = Field(...)
    
    class Config:
        schema_extra = {
            "example": {
                "username": "username",
                "password": "password"
            }
        }

class UserCredentials(SQLModel, table=True):
    id: int = Field(primary_key=True, default=None)
    email: str = Field(unique=True)
    username: str = Field(unique=True)
    password: str = Field(...)
    
    salary: "Salary" = Relationship(back_populates="user_credentials")
    
    class Config:
        schema_extra = {
            "example": {
                "id": 0,
                "email": "abc@index.com",
                "username": "username",
                "password": "password"
            }
        }

class Salary(SQLModel, table=True):
    id: int = Field(primary_key=True, default=None)
    user_id: int = Field(foreign_key="usercredentials.id", unique=True)
    salary: int = Field(...)
    date: str = Field(...)

    user_credentials: "UserCredentials" = Relationship(back_populates="salary")

    class Config:
        schema_extra = {
            "example": {
                "id": 0,
                "user_id": 0,
                "salary": 0,
                "date": "dd.mm.yyyy"
            }
        }


    @validator("date")
    def validate_date(cls, value):
        try:
            datetime.strptime(value, "%d.%m.%Y")
            return value
        except ValueError:
            raise ValueError("Invalid date format. Expected format: dd.mm.yyyy")
