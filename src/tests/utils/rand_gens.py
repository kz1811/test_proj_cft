import random
import string
from datetime import datetime, timedelta

def get_random_string(length: int = random.randint(5, 25)) -> str:

    return ''.join(random.choice(string.ascii_letters) for i in range(length))


def get_random_password_and_email(length: int = random.randint(5, 25)):
    lower_alphabet = list(string.ascii_lowercase)
    upper_alphabet = list(string.ascii_uppercase)
    cyrillic_alphabet = 'АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя'
    password = ''
    password += upper_alphabet[random.randint(0, len(upper_alphabet) - 1)]

    for i in range(5):
        password += lower_alphabet[random.randint(0, len(lower_alphabet) - 1)]
        password += str(random.randint(0, 9))
    password += cyrillic_alphabet[random.randint(0, len(cyrillic_alphabet) - 1)]

    email, domain, dot = '' + password[1], '', ''

    for i in range(length):
        email += lower_alphabet[random.randint(0, len(lower_alphabet) - 1)]
        domain += lower_alphabet[random.randint(0, len(lower_alphabet) - 1)]
    for i in range(3):
        dot = lower_alphabet[random.randint(0, len(lower_alphabet) - 1)]


    return {"password": password, "email": email+"@"+domain+"."+dot}

def get_random_number(starts_from: int = random.randint(0, 1000000), to: int|bool = None):
    if to is None:
        to = starts_from + random.randint(1, 10000)
    return random.randint(starts_from, to)

def get_random_date():
    now = datetime.now()
    random_days = random.randint(40, 1000)  # Генерация случайного количества дней
    random_date = now + timedelta(days=random_days)
    return random_date.strftime("%d.%m.%Y")