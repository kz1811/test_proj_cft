from starlette.testclient import TestClient
from operations.models import UserRegistrationModel
from main import app
from tests.utils.rand_gens import *

from sqlmodel import create_engine, SQLModel, Session, select, or_
from operations.models import UserLoginModel, UserCredentials, Salary, UserRegistrationModel
from operations.bd import engine


class TestCase():

    SQLModel.metadata.create_all(engine)

    def test_get_salary_by_token_from_registration(self, test_app):

        # Registration request

        username=get_random_string()
        password=get_random_password_and_email()['password']

        user_data = UserRegistrationModel(username=username,
                                          email=get_random_password_and_email()['email'],
                                          password=password,
                                          salary=get_random_number(),
                                          date=get_random_date())
        
        json_user_data = user_data.json()
        response = test_app.post("/api/user/register", data=json_user_data)
        assert response.status_code == 200, "Registration request :: Wrong status code"

        access_token = response.json().get("access_token")
        assert isinstance(access_token, str), "Registration request :: Token was not presented"

        # Get-salary request
        response = test_app.get("/api/get-salary", headers={"authorization": f"Bearer {access_token}"})
        assert response.status_code == 200, "Get salary :: Wrong status code"
        print(response.json())
        print(response.json()[0].get("salary"))
        print(user_data.salary)
        assert response.json()[0].get("salary") == user_data.salary, "Get salary :: Wrong response data"

        # Login request
        json_user_data = UserLoginModel(username=username, password=password).json()
        response = test_app.post("/api/user/login", data=json_user_data)
        assert response.status_code == 200, "Login request :: Wrong status code"

        access_token = response.json().get("access_token")
        assert isinstance(access_token, str), "Login request :: Token was not presented"

        # Get-salary request
        response = test_app.get("/api/get-salary", headers={"authorization": f"Bearer {access_token}"})
        assert response.status_code == 200, "Get salary :: Wrong status code"
        assert response.json()[0].get("salary") == user_data.salary, "Get salary :: Wrong response data"


    def test_negative_get_salary_by_invalid_token(self, test_app):

        wrong_token = get_random_string()

        # Get-salary request
        response = test_app.get("/api/get-salary", headers={"authorization": f"Bearer {wrong_token}"})
        assert response.status_code == 403, "Get salary :: Wrong status code"
        assert response.json().get("detail") == "Invalid token or expired token", "Get salary :: Wrong response data"

    def test_negative_login_by_non_existent_creds(self, test_app):
        
        # Login request
        json_user_data = UserLoginModel(username=get_random_string(20), password=get_random_string(20)).json()
        response = test_app.post("/api/user/login", data=json_user_data)
        assert response.status_code == 401, "Registration request :: Wrong status code"

        access_token = response.json().get("access_token")
        assert response.json().get("detail") == "Wrong username or password", "Login request :: Wrong response data"
