import uvicorn
from fastapi import HTTPException, status, FastAPI, Body, Depends, Header, Request
from typing import Annotated

from auth.auth_handler import *
from auth.auth_bearer import JWTBearer
from operations.models import UserLoginModel, UserCredentials, Salary, UserRegistrationModel

# db 
from operations.bd import engine
from sqlmodel import create_engine, SQLModel, Session, select, or_
from utils import *

app = FastAPI()

@app.on_event("startup")
def on_startup():
    SQLModel.metadata.create_all(engine)


# Register
@app.post("/api/user/register", tags=["user"])
async def register(user: UserRegistrationModel) -> dict:
    with Session(engine) as session:
        new_user = UserCredentials(username=user.username, 
                                            email=user.email, 
                                            password=bcrypt.hashpw(user.password.encode("utf-8"), bcrypt.gensalt()))

        if does_user_exists(username=user.username, email=user.email):
            raise HTTPException(status_code=422, detail="User with this email or username already exists")
        session.add(new_user)
        session.commit()
        session.refresh(new_user)

        new_salary_data = Salary(salary=user.salary, user_id=new_user.id, date=user.date)
        session.add(new_salary_data)
        session.commit()

        return signJWT(new_user.id)


# Login
@app.post("/api/user/login", tags=["user"])
async def login(user: UserLoginModel) -> dict:
    user_id = check_user(user)
    if user_id:
        return signJWT(user_id)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong username or password")


# Get salary data
@app.get("/api/get-salary",dependencies=[Depends(JWTBearer())], tags=["salary"])
async def get_salary(request: Request):

    ### Расшифровка JWT
    payload = decodeJWT(request.headers.get('Authorization').split()[-1])
    if not payload:
        raise HTTPException(status_code=403, detail="Invalid token or expired")
    if "user_id" not in payload:
        raise HTTPException(status_code=403, detail="Wrong token")

    ### Get user by id
    salary = get_salary_from_db(payload["user_id"])
    if salary:
        return salary
    raise HTTPException(status_code=404, detail="Not Found")


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5555, reload=True)
