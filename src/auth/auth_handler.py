import time
from typing import Dict
import jwt
from decouple import config

# Environment variables
JWT_SECRET = config("secret")
JWT_ALGORITHM = config("algorithm")

# Server response
def token_response(token: str):
    return {
        "access_token": token
    }

# Encoding token with jwt
def signJWT(user_id: str) -> Dict[str, str]:
    payload = {
        "user_id": user_id,
        "expires": time.time() + 60*10
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token_response(token)

# Decoding token with jwt
def decodeJWT(token: str) -> dict:
    try:
        decoded_token = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decoded_token if decoded_token["expires"] >= time.time() else None
    except:
        return {}